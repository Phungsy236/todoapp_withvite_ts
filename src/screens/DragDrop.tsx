import React, { DragEvent, useEffect, useState } from "react";
import MoveRightIcon from "../components/Icons/MoveRightIcon";
import { toast } from "react-hot-toast";
type Props = {};

const colors = ["red", "green", "blue", "pink", "yellow"];
// var colors = ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6', 
// 		  '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
// 		  '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A', 
// 		  '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
// 		  '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC', 
// 		  '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
// 		  '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680', 
// 		  '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
// 		  '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3', 
// 		  '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'];
function shuffleArray(array: any[]) {
    const temp = [...array];
    temp.sort(() => Math.random() - 0.5);
    return temp;
}
export default function DragDrop({}: Props) {
    const [point, setPoint] = useState(0);
    function handleDragStart(e: DragEvent<HTMLDivElement>) {
        e.dataTransfer.setData("ball", e.currentTarget.id);
    }
    const arrayColorLeft = React.useMemo(() => shuffleArray(colors), []);
    const arrayColorRight = React.useMemo(() => shuffleArray(colors), []);

    const containerLeft = React.useRef<HTMLDivElement>(null);
    const containerRight = React.useRef<HTMLDivElement>(null);
    function handleDrop(e: DragEvent<HTMLDivElement>, idDropInto: string) {
        const id = e.dataTransfer.getData("ball");
        const droppedElement = document.getElementById(id)!;
        const dropintoElement = document.getElementById(idDropInto)!;
        if (
            droppedElement?.id.replace("left-", "") === dropintoElement.id.replaceAll("right-", "")
        ) {
            setPoint((point) => point + 1);
            containerLeft.current!.removeChild(droppedElement);
            containerRight.current!.removeChild(dropintoElement);
        }
    }
    useEffect(() => {
        if (point === colors.length) toast.success("You win !!", { position: "top-center" });
    }, [point]);
    if (point === colors.length)
        return (
            <div className="mt-16 flex justify-center items-center">
                <button className="btn-primary" onClick={()=>window.location.reload()}>Chơi lại</button>
            </div>
        );
    return (
        <div className="px-[300px] py-8 flex justify-between items-center">
            <div className="flex flex-col gap-4" ref={containerLeft}>
                {arrayColorLeft.map((color, index) => (
                    <div
                        draggable
                        onDragStart={handleDragStart}
                        onDragOver={(e) => e.preventDefault()}
                        id={`left-ball-${color}`}
                        className="w-24 h-24 rounded-full cursor-pointer"
                        style={{ backgroundColor: color }}
                        key={index}
                    ></div>
                ))}
            </div>
            <MoveRightIcon className="w-24 h-24 text-slate-300"></MoveRightIcon>
            <div className="flex flex-col gap-4" ref={containerRight}>
                {arrayColorRight.map((color, index) => (
                    <div
                        onDragStart={handleDragStart}
                        onDragOver={(e) => e.preventDefault()}
                        id={`right-ball-${color}`}
                        onDrop={(e) => handleDrop(e, `right-ball-${color}`)}
                        className="w-24 h-24 rounded-full "
                        style={{ backgroundColor: color }}
                        key={index}
                    ></div>
                ))}
            </div>
        </div>
    );
}
