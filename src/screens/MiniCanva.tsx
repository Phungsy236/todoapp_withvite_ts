import React from "react";

type Props = {};

export default function MiniCanva({}: Props) {
    return (
        <div className="p-8">
            <p className="resize overflow-auto w-max h-max hover:border px-2 py-1 " contentEditable>MiniCanva</p>
        </div>
    );
}
