import * as React from "react";
import { connect } from "react-redux";
import { AppDispatch, RootState } from "store/store";

export interface IAppProps {}

class App extends React.Component<IAppProps & ReturnType<typeof mapState2Props>> {
    state = { name: "sy" };
    public render() {
        return (
            <div>
                {this.state.name}-{this.props.propsFromStore}
            </div>
        );
    }
}

const mapState2Props = (state: RootState) => {
    return {
        propsFromStore: state.appSlice.msg,
    };
};
const mapDispatch2Props = (dispatch: AppDispatch) => {
    return {
        setMsg: (payload: string) => dispatch({ type: "appSlice/setMsg", payload }),
    };
};
export default connect(mapState2Props, mapDispatch2Props)(App);
