import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import type { CardTodo, CardAssign, TODOSTATE } from '../typeDef/CardTodo'
import { arrayTodo, arrayUser } from './dataStore'

type APPSTATE = {
  showNotify: boolean;
  msg: string
}

const initialState: APPSTATE = {
  showNotify: false,
  msg: 'halo'
}
export const applice = createSlice({
  name: 'appSlice',
  initialState,
  reducers: {
    toggleSlice: (state, action: PayloadAction<boolean>) => {
      state.showNotify = action.payload
    },
    setMsg: (state, action: PayloadAction<string>) => {
      state.msg = action.payload
    }
  },
})
// Action creators are generated for each case reducer function
export const { toggleSlice , setMsg } = applice.actions
export default applice.reducer
